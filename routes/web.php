<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$base = "/api/v1/";

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function () {
    return \Illuminate\Support\Str::random(32);
});

$router->get("/check-age/{umur}", ['middleware' => 'checkAge', function ($umur) {
    return "Umur anda adalah " . $umur;
}]);

$router->post("auth/login", "AuthController@login");
$router->post("auth/register", "AuthController@register");


$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get("post/",  "PostController@index"); // index all
    $router->get("post/{id}", "PostController@detail"); // detail post
    $router->post("post/", "PostController@save"); // save new data
    $router->put("post/{id}", "PostController@update"); // update data
    $router->delete("post/{id}", "PostController@delete"); // delete
});
$router->get("post/download/pdf", "PostController@downloadPDF"); // download pdf
$router->get("post/download/excel", "PostController@downloadExcel"); // download excel
$router->get("post/store/excel", "PostController@storeExcel"); // download excel
