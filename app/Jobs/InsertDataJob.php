<?php

namespace App\Jobs;

use App\Models\Post;

class InsertDataJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Post::create(["user_id" => 1, "title" => "Post Schedule", "content" => "Konten Schedule"],);
        return "Inserted";
    }
}
