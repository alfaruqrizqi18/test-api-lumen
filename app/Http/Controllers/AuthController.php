<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");

        $hashPwd = Hash::make($password);

        $data = [
            "email" => $email,
            "password" => $hashPwd
        ];


        $user = User::create($data);
        if ($user) {
            $user['access_token'] =  $user->createToken($data['email'])->accessToken;
            $out = [
                "message" => "register_success",
                "code"    => 201,
                "data" => $user,
            ];
        } else {
            $out = [
                "message" => "failed_regiser",
                "code"   => 404,
                "data" => null,
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");

        $user = User::where("email", $email)->first();

        if (!$user) {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "data" => null,
            ];
            return response()->json($out, $out['code']);
        }

        if (Hash::check($password, $user->password)) {
            $url = 'http://localhost/belajarapilumen/public/oauth/token';
            $params = array(
                "client_id" => "2",
                "client_secret" => "JltAniexWQxa9GR3dBu6uu1yEIbyuFU789s8Gn5p",
                "username" => $email,
                "password" => $password,
                "grant_type" => "password"
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, constant("CURLOPT_" . 'URL'), $url);
            curl_setopt($ch, constant("CURLOPT_" . 'POST'), true);
            curl_setopt($ch, constant("CURLOPT_" . 'POSTFIELDS'), $params);
            $output = curl_exec($ch);
            curl_close($ch);
            $out = [
                "message" => "login_success",
                "code"    => 200,
                "data"  => $user,
                "token" => json_decode($output, true)
            ];
        } else {
            $out = [
                "message" => "password_not_match",
                "code"    => 401,
                "data"  => null
            ];
        }

        return response()->json($out, $out['code']);
    }

    //
}
