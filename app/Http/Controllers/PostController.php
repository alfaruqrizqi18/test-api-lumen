<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PostsExport;
use PDF;
use PhpParser\Node\Expr\AssignOp\Concat;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $posts = Post::with('user')->orderBy('id', 'desc')->simplePaginate(10);
        $return = array(
            "message" => "OK",
            "code" => 200,
            "data" => $posts,
        );
        return response()->json($return, $return['code']);
    }

    public function detail($id)
    {
        $posts = Post::with('user')->find($id);
        if ($posts != "") {
            $return = [
                "message" => "OK",
                "code" => 200,
                "data" => $posts,
            ];
        } else {
            $return = [
                "message" => "OK",
                "code" => 200,
                "data" => [],
            ];
        }
        return response()->json($return, $return['code']);
    }

    public function save(Request $request)
    {

        $user_id = Auth::user()->id;
        $title = $request->input('title');
        $content = $request->input('content');

        DB::beginTransaction();
        try {
            DB::insert('insert into posts (user_id, title, content, created_at, updated_at) values (?, ?, ?, ?, ?)', [$user_id, $title, $content, Carbon::now(), Carbon::now()]);
            DB::commit();
            $return = [
                "message" => "OK",
                "code" => 200,
            ];
            return response()->json($return, $return['code']);
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            $return = [
                "message" => "INSERT_FAILED : " . $e,
                "code" => 200,
            ];
            return response()->json($return, $return['code']);
            // something went wrong
        }

        //save to database
        // $save = Post::create(["user_id" => $user_id, "title" => $title, "content" => $content],);

        //get detail data post after save
        // $detail_post = Post::with('user')->find($save->id);

        // if ($save) {
        //     $return = [
        //         "message" => "OK",
        //         "code" => 200,
        //         "data" => $detail_post,
        //     ];
        // } else {

        //     $return = [
        //         "message" => "FAILED_INSERT",
        //         "code" => 400,
        //         "data" => null,
        //     ];
        // }
    }

    public function update(Request $request, $id)
    {
        // return $input = $request->all();
        $post = Post::where('id', $id)->update(['title' => $request->input('title'), 'content' => $request->input('content')]);
        if ($post) {
            $return = [
                "message" => "OK",
                "code" => 200,
            ];
        } else {
            $return = [
                "message" => "FAILED_UPDATE",
                "code" => 400,
            ];
        }
        return response()->json($return, $return['code']);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        if ($post) {
            $post->delete();
            $return = [
                "message" => "OK",
                "code" => 200,
            ];
        } else {
            $return = [
                "message" => "NO_DATA_FOUND",
                "code" => 400,
            ];
        }
        return response()->json($return, $return['code']);
    }

    public function downloadPDF()
    {
        $post = Post::with('user')->orderBy('id', 'desc');
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadHTML(view('data')->with('name', $post->get()));
        return $pdf->inline();
        // return view('data')->with('name', $post->get());
    }

    public function downloadExcel()
    {
        $date_now = Carbon::now();
        return Excel::download(new PostsExport, $date_now . '-POST.xlsx',);
    }

    public function storeExcel()
    {
        $date_now = Carbon::now();
        $filename = sha1($date_now) . '-POST.xlsx';
        Excel::store(new PostsExport, $filename);
        $return = [
            "message" => "OK",
            "code" => 400,
            "file_name" => $filename,
            "url_file" => storage_path() . "\\" . $filename,
        ];
        return response()->json($return, $return['code']);
    }
}
