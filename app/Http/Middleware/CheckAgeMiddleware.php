<?php

namespace App\Http\Middleware;

use Closure;

class CheckAgeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->umur <= 20) {
            return "Anda tidak di ijinkan masuk, karena umur anda belum mencukupi. => " . $request->umur;
        }
        return $next($request);
    }
}
